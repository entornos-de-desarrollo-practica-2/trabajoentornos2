
//import java.util.*;
/**
 * Write a description of class Stats here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Stats
{
    // Declaración de variables
    int Vida;
    int Resistencia;
    int Magia;
    int Fuerza;
    int Edad;
    int Aprendizaje;
    int Armadura;
    int Destreza;
    int Fe;
    int Raza;

    public Stats()
    {
        // Declaración de las estadísticas base del personaje máximas
        Vida=600;
        Resistencia=300;
        Magia=100;
        Fuerza=100;
        Edad=100;
        Aprendizaje=10;
        Armadura=100;
        Destreza=10;
        Fe=100;
        Raza=4;
    }
    
    
    public Stats(int $Vida, int $Resistencia, int $Magia, int $Fuerza, int $Edad, int $Aprendizaje, int $Armadura, int $Destreza,
    int $Fe, int $Raza) 
    {
        // Declaración de las variables de las estadísticas
        Vida=$Vida;
        Resistencia=$Resistencia;
        Magia=$Magia;
        Fuerza=$Fuerza;
        Edad=$Edad;
        Aprendizaje=$Aprendizaje;
        Armadura=$Armadura;
        Destreza=$Destreza;
        Fe=$Fe;
        Raza=$Raza;
    }
                 
    
    public String dimeEstado()
    {   
        if ((Vida == 0)) 
            return "Has muerto";
        if ((Resistencia == 0) && (Magia == 0))
            return "Necesitas descansar y no tienes magia";
        if ((Resistencia == 0))
            return "Necesitas descansar";
        if ((Magia == 0))
            return "No tienes magia, tomate una pocion de magia";
        if ((Vida <= 300) && (Vida != 0) && (Resistencia <= 150) && (Resistencia != 0) && (Magia <= 50) && (Magia != 0)) 
            return "Te estas quedando sin vida, sin resistencia y sin magia";            
        if ((Vida <= 300) && (Vida != 0) && (Resistencia <= 150) && (Resistencia != 0))
            return "Te estas quedando sin vida y sin resistencia";
        if ((Vida <= 300) && (Vida != 0) && (Magia <= 50) && (Magia != 0))
            return "Te estas quedando sin vida y sin magia";
        if ((Resistencia <= 150) && (Resistencia != 0) && (Magia <= 50) && (Magia != 0))
            return "Te estas quedando sin resistencia y sin magia";
        if ((Vida < 0))
            return "No se pueden introducir negativos";
        if ((Resistencia < 0))
            return "No se pueden introducir negativos";
        if ((Magia < 0))
            return "No se pueden introducir negativos";
        if ((Vida > 600))
            return "Valor MAX Vida invalido";
        if ((Resistencia > 300))
            return "Valor MAX Resistencia invalido";   
        if ((Magia > 100))
            return "Valor MAX Magia invalido";
        if ((Vida <= 300) && (Vida != 0))
            return "Te estas quedando sin vida, tomate una pocion de vida";
        if ((Resistencia <= 150) && (Resistencia != 0))
            return "Te estas quedando sin resistencia, descansa";
        if ((Magia <= 50) && (Magia != 0))
            return "Te estas quedando sin magia, tomate una pocion de magia";
        if ((Vida <= 300) && (Vida != 0))
            return "Te estas quedando sin vida, tomate una pocion de vida";
        if ((Resistencia <= 150) && (Resistencia != 0))
            return "Te estas quedando sin resistencia, descansa";
        if ((Magia <= 50) && (Magia !=0 ))
            return "Te estas quedando sin magia, tomate una pocion de magia";
        else
            return "Todo ok";
    }
    
    public String dimeVida()
    { 
      if ((Vida == 0)) 
            return "Has muerto";
      if ((Vida < 0))
            return "No se pueden introducir negativos";
      if ((Vida > 600))
            return "Valor MAX Vida invalido";
      if ((Vida <= 300) && (Vida != 0))
            return "Te estas quedando sin vida, tomate una pocion de vida";
      else
            return "Todo ok";
    }
    
    public String dimeResistencia()
    { 
      if ((Resistencia == 0))
            return "Necesitas descansar";
      if ((Resistencia < 0))
            return "No se pueden introducir negativos";
      if ((Resistencia > 300))
            return "Valor MAX Resistencia invalido";
      if ((Resistencia <= 150) && (Resistencia != 0))
            return "Te estas quedando sin resistencia, descansa";
      else
            return "Todo ok";
    }
    
    public String dimeMagia()
    { 
      if ((Magia == 0))
            return "No tienes magia, tomate una pocion de magia";
      if ((Magia < 0))
            return "No se pueden introducir negativos";
      if ((Magia > 100))
            return "Valor MAX Magia invalido";
      if ((Magia<=50) && (Magia!=0))
            return "Te estas quedando sin magia, tomate una pocion de magia";
      else
            return "Todo ok";
    }
    
    public String dimeFuerza()
    { 
      if ((Fuerza < 0))
            return "No se pueden introducir negativos";
      if ((Fuerza > 100))
            return "Valor MAX Fuerza invalido";
      else
            return "Todo ok";
    }
    
    public String dimeEdad()
    {
      if ((Edad < 0))
            return "No se pueden introducir negativos";
      if ((Edad >= 18) && (Edad < 30))
            return "Has elegido un personaje joven";
      if ((Edad >= 30) && (Edad < 60))
            return "Has elegido un personaje adulto";
      if ((Edad >= 60) && (Edad < 100))
            return "Has elegido un personaje viejo";
      if ((Edad > 100))
            return "Valor MAX Edad invalido";
      else
            return "Todo ok";
    }
    
    public String dimeAprendizaje()
    { 
      if ((Aprendizaje < 0))
            return "No se pueden introducir negativos";
      if ((Aprendizaje > 10))
            return "Valor MAX Aprendizaje invalido";
      else
            return "Todo ok";
    }    

    public String dimeArmadura()
    {
      if ((Armadura > 100))
            return "Valor MAX Armadura invalido";
      if ((Armadura==0))
            return "No tienes armadura";
      if ((Armadura < 30) && (Armadura > 0))
            return "Tienes armadura ligera";
      if ((Armadura < 70) && (Armadura >= 30))
            return "Tienes armadura de peso medio";
      if ((Armadura <= 100) && (Armadura >= 70))
            return "Tienes armadura pesada";
      if ((Armadura < 0))
            return "No se pueden introducir negativos";
      else 
            return "Todo ok";
    }
           
    public String dimeDestreza()
    {
      if ((Destreza > 10))
            return "Valor MAX Destreza invalido";
      if ((Destreza < 0))
            return "No se pueden introducir negativos";
      if ((Destreza <= 3) && (Destreza > 0))
            return "Tu nivel de destreza es bajo";
      if ((Destreza <= 7) && (Destreza > 3))
            return "Tu nivel de destreza es medio";
      if ((Destreza <= 10) && (Destreza >=8))
            return "Tu nivel de destreza es alto";
      else
            return "Todo ok";
    }
    
    public String dimeFe()
    {
      if ((Fe > 100))
            return "Valor MAX Fe invalido";
      if ((Fe < 0))
            return "No se pueden introducir negativos";
      if ((Fe <= 50 ) && (Fe > 0))
            return "Eres creyente";
      if ((Fe <= 100) && (Fe <51))
            return "No eres creyente";
      else 
            return "Todo ok";
    }
    
    public String dimeRaza()
    {
        if ((Raza > 4))
            return "Valor MAX Raza invalido";
        if ((Raza < 0))
            return "No se pueden introducir negativos";
        if ((Raza == 1))
            return "Eres un Alien";
        if ((Raza == 2))
            return "Eres un Humano";
        if ((Raza == 3))
            return "Eres un Pokemon";
        if ((Raza == 4))
            return "Eres un Sayan";
        else 
            return "Elige personaje";
        }
        
        
    // Esto sirve para obtener una por una las estadísticas del personaje
    
    public int getVIDA(){
        return Vida;
    }
    
    public int getRESISTENCIA(){
        return Resistencia;
    }
    
    public int getMAGIA(){
        return Magia;
    }
    
    public int getFUERZA(){
        return Fuerza;
    }
    
    public int getEDAD(){
        return Edad;
    }
    
    public int getAPRENDIZAJE(){
        return Aprendizaje;
    }
    
    public int getARMADURA(){
        return Armadura;
    }
        
    public int getDESTREZA(){
        return Destreza;
    }
    
    public int getFE(){
        return Fe;
    }
    
    public int getRAZA(){
        return Raza;
    }
       
    // Esto sirve para poder cambiar una por una las estadísticas del personaje
    public void setVIDA(int num){
        Vida=num;
    }
    
    public void setRESISTENCIA(int num){
        Resistencia=num;
    }
    
    public void setMAGIA(int num){
        Magia=num;
    }
    
    public void setFUERZA(int num){
        Fuerza=num;
    }
    
    public void setEDAD(int num){
        Edad=num;
    }
    
    public void setAPRENDIZAJE(int num){
        Aprendizaje=num;
    }
    
    public void setARMADURA(int num){
        Armadura=num;
    }
        
    public void setDESTREZA(int num){
        Destreza=num;
    }
    
    public void setFE(int num){
        Fe=num;
    }
    
    public void setRAZA(int num){
        Raza=num;
    }
}

