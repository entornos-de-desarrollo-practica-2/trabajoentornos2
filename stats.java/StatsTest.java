
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StatsTest
{
    Stats s;
    public StatsTest()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }
    
    @Test
    public void prueba1()//vida > 300 y <=600 - resistencia >150 y <=300 - magia >50 y <=100
    {
        s = new Stats(400,200,60,70,30,7,10,10,70,4);
        assertEquals("Todo ok",s.dimeEstado());
    }
    
    @Test
    public void prueba2() //vida = 0
    {
        s = new Stats(0,200,70,30,30,7,10,10,70,4);
        assertEquals("Has muerto",s.dimeEstado());
    }
    
    @Test
    public void prueba3() //resistencia = 0
    {
        s = new Stats(400,0,70,30,30,7,10,10,70,4);
        assertEquals("Necesitas descansar",s.dimeEstado());
    }
    
    @Test
    public void prueba4() //magia = 0
    {
        s = new Stats(400,200,0,30,30,7,10,10,70,4);
        assertEquals("No tienes magia, tomate una pocion de magia",s.dimeEstado());
    }   
    
    @Test
    public void prueba5()//vida <=300 magia <=50
    {
        s = new Stats(100,200,20,30,30,7,10,10,70,4);
        assertEquals("Te estas quedando sin vida y sin magia",s.dimeEstado());
    }
      
    @Test
    public void prueba6()//resistencia <=150 magia <=50
    {
        s = new Stats(400,50,20,30,30,7,10,10,70,4);
        assertEquals("Te estas quedando sin resistencia y sin magia",s.dimeEstado());
    }
    
    @Test
    public void prueba7()//vida <=300 resistencia <=150 magia <=50
    {
        s = new Stats(100,50,20,30,30,7,10,10,70,4);
        assertEquals("Te estas quedando sin vida, sin resistencia y sin magia",s.dimeEstado());
    }
    
    @Test
    public void prueba8()//vida < 0
    {
        s = new Stats(-400,200,70,30,30,7,10,10,70,4);
        assertEquals("No se pueden introducir negativos",s.dimeEstado());
    }
        
    @Test
    public void prueba9()//vida <=300 resistencia <=150
    {
        s = new Stats(100,50,70,30,30,7,10,10,70,4);
        assertEquals("Te estas quedando sin vida y sin resistencia",s.dimeEstado());
    }
    
    @Test
    public void prueba10()//vida <=300 
    {
        s = new Stats(50,200,70,30,30,7,10,10,70,4);
        assertEquals("Te estas quedando sin vida, tomate una pocion de vida",s.dimeEstado());
    }
    
    @Test
    public void prueba11()//resistencia <=150
    {
        s = new Stats(400,70,70,30,30,7,10,10,70,4);
        assertEquals("Te estas quedando sin resistencia, descansa",s.dimeEstado());
    }
    
    @Test
    public void prueba12()//magia <=50 
    {
        s = new Stats(400,200,20,30,30,7,10,10,70,4);
        assertEquals("Te estas quedando sin magia, tomate una pocion de magia",s.dimeEstado());
    }
    
    @Test
    public void prueba13()//vida > 600
    {
        s = new Stats(1000,200,70,30,30,7,10,10,70,4);
        assertEquals("Valor MAX Vida invalido",s.dimeEstado());
    }
    
    @Test
    public void prueba14()//resitencia > 300
    {
        s = new Stats(400,400,70,30,30,7,10,10,70,4);
        assertEquals("Valor MAX Resistencia invalido",s.dimeEstado());
    }
    
    @Test
    public void prueba15()//magia > 100
    {
        s = new Stats(400,200,200,30,30,7,10,10,70,4);
        assertEquals("Valor MAX Magia invalido",s.dimeEstado());
    }
    
    @Test
    public void prueba16()//resistencia < 0
    {
        s = new Stats(400,-100,70,30,30,7,10,10,70,4);
        assertEquals("No se pueden introducir negativos",s.dimeEstado());
    }
    
    @Test
    public void prueba17()//magia < 0
    {
        s = new Stats(400,200,-60,30,30,7,10,10,70,4);
        assertEquals("No se pueden introducir negativos",s.dimeEstado());
    }
    
    @Test
    public void prueba18()//fuerza < 0
    {
        s = new Stats(400,200,70,-30,30,7,10,10,70,4);
        assertEquals("No se pueden introducir negativos",s.dimeFuerza());
    }
    
    @Test
    public void prueba19()//fuerza > 100
    {
        s = new Stats(400,200,70,200,30,7,10,10,70,4);
        assertEquals("Valor MAX Fuerza invalido",s.dimeFuerza());
    }
    
    @Test
    public void prueba20() //edad > 100
    {
        s = new Stats(400,200,70,30,109,7,10,10,70,4);
        assertEquals("Valor MAX Edad invalido",s.dimeEdad());
    }
    
    @Test
    public void prueba21() //edad < 0
    {
        s = new Stats(400,200,70,30,-18,7,10,10,70,4);
        assertEquals("No se pueden introducir negativos",s.dimeEdad());
    }
    
    @Test
    public void prueba22() //edad < 0
    {
        s = new Stats(400,200,70,30,24,7,10,10,70,4);
        assertEquals("Has elegido un personaje joven",s.dimeEdad());
    }
    
    @Test
    public void prueba23() //edad < 0
    {
        s = new Stats(400,200,70,30,50,7,10,10,70,4);
        assertEquals("Has elegido un personaje adulto",s.dimeEdad());
    }
    
    @Test
    public void prueba24() //edad < 0
    {
        s = new Stats(400,200,70,30,82,7,10,10,70,4);
        assertEquals("Has elegido un personaje viejo",s.dimeEdad());
    }
    
    @Test
    public void prueba25() //aprendizaje < 0
    {
        s = new Stats(400,200,70,30,30,-7,10,10,70,4);
        assertEquals("No se pueden introducir negativos",s.dimeAprendizaje());
    }
    
    @Test
    public void prueba26() //aprendizaje > 10
    {
        s = new Stats(400,200,70,30,30,11,10,10,70,4);
        assertEquals("Valor MAX Aprendizaje invalido",s.dimeAprendizaje());
    }
    
    @Test
    public void prueba27()//armadura > 100
    {
        s = new Stats(400,200,70,30,30,7,120,10,70,4);
        assertEquals("Valor MAX Armadura invalido",s.dimeArmadura());
    }
    
    @Test
    public void prueba28()//armadura = 0
    {
        s = new Stats(400,200,70,30,30,7,0,10,70,4);
        assertEquals("No tienes armadura",s.dimeArmadura());
    }
    
    @Test
    public void prueba29()//armadura < 0
    {
        s = new Stats(400,200,70,30,30,7,-20,10,70,4);
        assertEquals("No se pueden introducir negativos",s.dimeArmadura());
    }
    
    @Test
    public void prueba30()//Armadura < 30 y Armadura > 0
    {
        s = new Stats(400,200,70,30,30,7,20,10,70,4);
        assertEquals("Tienes armadura ligera",s.dimeArmadura());
    }
    
    @Test
    public void prueba31()//Armadura < 70 y Armadura >= 30
    {
        s = new Stats(400,200,70,30,30,7,50,10,70,4);
        assertEquals("Tienes armadura de peso medio",s.dimeArmadura());
    }
    
    @Test
    public void prueba32()//Armadura < 100 y Armadura >= 70
    {
        s = new Stats(400,200,70,30,30,7,90,10,70,4);
        assertEquals("Tienes armadura pesada",s.dimeArmadura());
    }
    
    @Test
    public void prueba33() //destreza > 10
    {
        s = new Stats(400,200,70,30,30,8,12,20,70,4);
        assertEquals("Valor MAX Destreza invalido",s.dimeDestreza());
    }
    
    @Test
    public void prueba34() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,-8,70,4);
        assertEquals("No se pueden introducir negativos",s.dimeDestreza());
    }
    
    @Test
    public void prueba35() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,2,70,4);
        assertEquals("Tu nivel de destreza es bajo",s.dimeDestreza());
    }
    
    @Test
    public void prueba36() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,5,70,4);
        assertEquals("Tu nivel de destreza es medio",s.dimeDestreza());
    }
    
    @Test
    public void prueba37() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,70,4);
        assertEquals("Tu nivel de destreza es alto",s.dimeDestreza());
    }
    
    @Test
    public void prueba38() //fe < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,-8,4);
        assertEquals("No se pueden introducir negativos",s.dimeFe());
    }
    
    @Test
    public void prueba39() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,200,4);
        assertEquals("Valor MAX Fe invalido",s.dimeFe());
    }
    
    @Test
    public void prueba40() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,40,4);
        assertEquals("Eres creyente",s.dimeFe());
    }
    
    @Test
    public void prueba41() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,70,4);
        assertEquals("No eres creyente",s.dimeFe());
    }
    
    @Test
    public void prueba42() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,70,-5);
        assertEquals("No se pueden introducir negativos",s.dimeRaza());
    }
    
    @Test
    public void prueba43() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,70,7);
        assertEquals("Valor MAX Raza invalido",s.dimeRaza());
    }
    
    @Test
    public void prueba44() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,70,1);
        assertEquals("Eres un Alien",s.dimeRaza());
    }
    
    @Test
    public void prueba45() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,70,2);
        assertEquals("Eres un Humano",s.dimeRaza());
    }
    
    @Test
    public void prueba46() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,70,3);
        assertEquals("Eres un Pokemon",s.dimeRaza());
    }
    
    @Test
    public void prueba47() //destreza < 0
    {
        s = new Stats(400,200,70,30,30,7,10,8,70,4);
        assertEquals("Eres un Sayan",s.dimeRaza());
    }
}
